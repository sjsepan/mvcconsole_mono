using System;
using System.Collections.Generic;
using System.Text;
using Ssepan.Utility;

namespace Ssepan.Application
{
    public static class GenericApplication 
    {
        #region Declarations
        public delegate void Action(); //Action<void>, AKA Action
        public delegate void Action<T1, T2>(T1 arg1, T2 arg2);//Action<T1,T2>
        #endregion Declarations

        #region Constructors
        static GenericApplication()
        {
        }
        #endregion Constructors

        #region Properties
        #endregion Properties

        #region Public Methods
        #endregion Public Methods

        #region Private Methods
        #endregion Private Methods
    }
}